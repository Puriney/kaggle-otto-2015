# kaggle-otto-2015
My source codes for Otto group challenge 2015. The winning models will be open sourced.

# Problem
A consistent analysis of the performance of our products is crucial. However, due to our diverse global infrastructure, many identical products get classified differently. 

# Goal
LSH based approach to solve Entity Resolution problem. 

# Data
For this competition, we have provided a dataset with 93 features for more than 200,000 products.

Each row corresponds to a single product. There are a total of 93 numerical features, which represent counts of different events. There are nine categories for all products. Each target category represents one of our most important product categories (like fashion, electronics, etc.). The products for the training and testing sets are selected randomly.

 
