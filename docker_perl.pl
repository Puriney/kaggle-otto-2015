#!/usr/bin/perl -w 

my $n = 144368; ## total lines number of test file
my $py_path = "./LSH.py"; 
my $bash_pre = "#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=10G
set -e
module load python/2.7_Anaconda
module load perl/PM_5.10.1

sleep 1s
cd /g1/home/yy332/CS562/kaggle-otto-2015\n"; 

print $bash_pre; 

my $newS = 0; 
my $step = 2000; 
my $newE = 0; 
my $i = 0; 
while(1){
	$newE = $newS + $step; 
	last if $newE > $n; 

	print $newS . "~" . $newE . "\n"; 
	## perform code + batch
	my $py_out = "./LSH_token" . $i . ".py"; 
	my $bash_out = "./LSH_token_sub" . $i . ".sh"; 

	open IN, $py_path; 
	open OUT, ">$py_out"; 
	while (<IN>) {
		chomp; 
		my $line = $_; 
		if ($line =~ m/fix start index/) { 
			$line =~ s/(\d)+/$newS/g; 
		}
		elsif ($line =~ m/fix end index/) {
			$line =~ s/(\d)+/$newE/g; 
		}
		else {
		}
		print OUT "$line\n"; 
	}
	close IN; 
	close OUT;

	open OUTJOB, ">$bash_out"; 
	print OUTJOB $bash_pre; 
	print OUTJOB "python $py_out \n"; 
	print OUTJOB "sleep 1s"; 
	close OUTJOB; 

	`sbatch $bash_out`; 
	##

	$newS = $newE; # next round
	$i = $i + 1; 
}

$i = $i + 5; 
print $newE . "\n"; 
if ($newE > $n){
	if ($newS < $n){
		print $newS . "~" . $newE . "\n"; 
		## perform code + batch
		my $py_out = "./LSH_token" . $i . ".py"; 
		my $bash_out = "./LSH_token_sub" . $i . ".sh"; 

		open IN, $py_path; 
		open OUT, ">$py_out"; 
		while (<IN>) {
			chomp; 
			my $line = $_; 
			if ($line =~ m/fix start index/) { 
				$line =~ s/(\d)+/$newS/g; 
			}
			elsif ($line =~ m/fix end index/) {
				$line =~ s/(\d)+/$n/g; 
			}
			else {
			}
			print OUT "$line\n"; 
		}
		close IN; 
		close OUT;

		open OUTJOB, ">$bash_out"; 
		print OUTJOB $bash_pre; 
		print OUTJOB "python $py_out \n"; 
		print OUTJOB "sleep 1s"; 
		close OUTJOB; 

		`sbatch $bash_out`; 
		##
	}
}
