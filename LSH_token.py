#!/usr/bin/python

# coding: utf-8

# In[158]:

import time
import pandas as pd
import numpy as np
# import matplotlib.pyplot as plt
# %matplotlib inline
import scipy.sparse as sparse
import scipy.stats as stats
from sklearn.neighbors import LSHForest
import random
from sklearn.preprocessing import StandardScaler


# In[179]:
start_time = str(time.strftime("%m%d%Y-%H%M%S"))
print "Start at " + start_time


# In[160]:

## ------------------------
## Read-in Data
## ------------------------
f_train0 = pd.read_csv("./raw/train.csv")
f_test0  = pd.read_csv("./raw/test.csv")


# In[180]:

## ------------------------
## [optional] Sample data for codes test
## ------------------------
class_labels = sorted(list(set(f_train0['target'])))
# k = 10
s = 0 ## Use Perl to fix start index
e = 2000 ## Use Perl to fix end index
sample_idx = range(s, e)
# f_train = f_train0.iloc[sample_idx, :]
f_test = f_test0.iloc[sample_idx, :]

f_train = f_train0
# f_test = f_test0


# In[ ]:

f_train.set_index('id', inplace=True)
f_test.set_index('id', inplace=True)


# In[181]:

## ------------------------
## Preprosessing Data (scalling)
## ------------------------
m_train_tmp = f_train.drop('target', 1)
for x in (m_train_tmp.columns):
	m_train_tmp[x] = m_train_tmp[x].astype('float64')
for x in (f_test.columns): 
	f_test[x] = f_test[x].astype('float64')
m_train = StandardScaler().fit_transform(m_train_tmp)
m_test = StandardScaler().fit_transform(f_test)


# In[189]:

## ------------------------
## Set up LSH and apply for train data
## ------------------------
lshf = LSHForest(min_hash_match=4)
lshf.fit(m_train)


# In[190]:

## ------------------------
## Fit test data
## ------------------------
distances, indices = lshf.radius_neighbors(m_test, radius=1.)

# In[206]:

## ------------------------
## Mapping class labels from train to fitted test data
## ------------------------
f_out_r = f_test.shape[0]
f_out_c = len(class_labels)
f_out = np.asmatrix(np.zeros((f_out_r, f_out_c)))
## Naive scheme: counting the amount of labels of similar items. 

for i in xrange(len(indices)): 
    ## each item
    if len(indices[i]) == 0: 
        continue
    neighbor_trains  = f_train.iloc[indices[i], :]
    neighbor_classes = neighbor_trains['target'].tolist()
    nb_cls_cnt = [neighbor_classes.count(x) for x in class_labels]
    nb_cls_prt = [float(x) / sum(nb_cls_cnt) for x in nb_cls_cnt]
    f_out[i, :] = nb_cls_prt
    
f_out = pd.DataFrame(f_out, columns=class_labels, index = f_test.index)


# In[ ]:

## ------------------------
## Mapping class labels from train to fitted test data
## (parallel computing)
## ------------------------
# def kissNeighbors(test_data, train_data, out_data, class_lab, i):
#     f_train = train_data
#     f_test = test_data
#     class_labels = class_lab
#     indices = ind
    
#     f_out_r = f_test.shape[0]
#     f_out_c = len(class_labels)
#     f_out = np.asmatrix(np.zeros((f_out_r, f_out_c)))
#     ## Naive scheme: counting the amount of labels of similar items. 
#     for i in xrange(len(indices)): 
#         ## each item
#         if len(indices[i]) == 0: 
#             continue
#         neighbor_trains  = f_train.iloc[indices[i], :]
#         neighbor_classes = neighbor_trains['target'].tolist()
#         nb_cls_cnt = [neighbor_classes.count(x) for x in class_labels]
#         nb_cls_prt = [float(x) / sum(nb_cls_cnt) for x in nb_cls_cnt]
#         f_out[i, :] = nb_cls_prt

#     f_out = pd.DataFrame(f_out, columns=class_labels, index = f_test.index)


# In[207]:

##
## Post-processing
##
f_out = pd.DataFrame(f_out, columns=class_labels, index = f_test.index)
end_time = str(time.strftime("%H%M%S"))
f_out_path = './output/LSH-' +  start_time + '-' + end_time + '.csv' 
f_out.to_csv(f_out_path)


# In[208]:

print "Finish at " + time.strftime("%m%d%Y-%H%M%S")


# In[210]:

# x = [0] * 10
# print x
# def myfun (mylist, i):
    


# In[211]:

# [x[i] = i**2 for i in xrange(len(x))]


# In[ ]:



